#!/bin/bash

SSID=yourwifiname
PASSWORD=yourwifipassowrd

while /bin/true; do
    if ! [ "$(ping -c 1 puscii.nl)" ]; then
        echo "Warning: connection lost at $(date) -- restart"
        nmcli con down "$SSID"
        sleep 5

        nmcli r wifi on
        nmcli device wifi rescan
        sleep 5
        nmcli device wifi connect "$SSID" password "$PASSWORD"

        sleep 60
        if ! [ "$(ping -c 1 puscii.nl)" ]; then
             echo "Waiting for connection going up at $(date)"
             sleep 60
        else
             echo "Connection on at $(date)"
        fi
    fi
sleep 60
done
