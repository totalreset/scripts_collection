#!/usr/bin/env python3

## Converts image and music files into video with image 
import os

if "Videos" not in os.listdir("."):
    os.mkdir("Videos")

def converter(format):
    """Converts audio files into video with a cover.jpg image using ffmpeg"""
    print("Converting " + f + " into video...")
    exit_status = os.system("ffmpeg -loop 1 -i cover.jpg -i '" + f +
    "' -c:v libx264 -strict experimental -b:a 256k -shortest Videos/'" +
    f.replace(format, "") + "'.mp4")
    print("Process terminated with status: " + str(exit_status) )

for f in os.listdir("."):
    if ".wav" in f:
        converter(".wav")
    elif ".flac" in f:
        converter(".flac")

print("Terminating script. Bye")
